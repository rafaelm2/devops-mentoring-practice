import React from 'react';
import Quiz from './components/Quiz';

function App() {
  return (
    <div className="app">
      <h1 className='quiz-title'>Quiz BTS</h1>
      <Quiz />
    </div>
  );
}

export default App;
