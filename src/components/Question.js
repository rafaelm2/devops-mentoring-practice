import React from 'react';

class Question extends React.Component {
  render() {
    const { question, answers, userAnswer, onAnswerSelect } = this.props;

    return (
      <div>
        <h2>{question}</h2>
        <div className="answers-container">
          {answers.map((answer, index) => (
            <button
              key={index}
              onClick={() => onAnswerSelect(index)}
              className={`answer-button ${userAnswer !== undefined && userAnswer === index ? 'selected' : ''}`}
            >
              {answer}
            </button>
          ))}
        </div>
      </div>
    );
  }
}

export default Question;
