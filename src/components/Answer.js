import React from 'react';

const Answer = ({ answer, isSelected, onSelect }) => {
  return (
    <div
      style={{ backgroundColor: isSelected ? 'lightblue' : 'white' }}
      onClick={onSelect}
    >
      {answer}
    </div>
  );
};

export default Answer;
